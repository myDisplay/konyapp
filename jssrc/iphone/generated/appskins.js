function skinsInit() {
    defBtnFocus = "defBtnFocus";
    defBtnNormal = "defBtnNormal";
    defLabel = "defLabel";
    defTextBoxFocus = "defTextBoxFocus";
    defTextBoxNormal = "defTextBoxNormal";
    defTextBoxPlaceholder = "defTextBoxPlaceholder";
    sknLblDescription = "sknLblDescription";
    sknLblRowHeading = "sknLblRowHeading";
    sknLblStrip = "sknLblStrip";
    sknLblTimeStamp = "sknLblTimeStamp";
    sknSampleRowTemplate = "sknSampleRowTemplate";
    sknSampleSectionHeaderTemplate = "sknSampleSectionHeaderTemplate";
    sknSectionHeaderLabelSkin = "sknSectionHeaderLabelSkin";
    slDynamicNotificationForm = "slDynamicNotificationForm";
    slFbox = "slFbox";
    slForm = "slForm";
    slPopup = "slPopup";
    slSplashiOS = "slSplashiOS";
    slStaticNotificationForm = "slStaticNotificationForm";
    slTitleBar = "slTitleBar";
    slWatchForm = "slWatchForm";
};